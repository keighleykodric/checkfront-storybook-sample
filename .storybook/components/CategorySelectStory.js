import React from 'react';
import { storiesOf, action, decorateAction } from '@storybook/react';
import Button from '../../src/components/Button';
import Category from '../../src/components/Category';

const buttonEvents = {
  onClick: action('onClick'),
  onFocus: action('onFocus'),
  className: 'new-class',
};

const categoryEvents = {
  onClick: action('onClick'),
  onFocus: action('onFocus'),
  className: 'category--sel',
};

// const firstArgAction = decorateAction([
//   args => args.slice(0, 1)
// ]);

storiesOf('Categories', module)
  .addWithInfo(
    'Category Selector',
    `
      The example below shows the Category Selector component in the default state.
    `,
    () =>
      <div><h1>Category Selector</h1>
        <Category {...categoryEvents} className="" >
          Demo
        </Category>
        <Category {...categoryEvents} className="" >
          Campsites
        </Category>
        &nbsp;
        <Button {...buttonEvents} className="some-class" kind="secondary">Create Category</Button>
        &nbsp;
      </div>
  )
  .addWithInfo(
    'Category Creator',
    `
      The example below shows the Category Selector component in the Create Category state.
    `,
    () =>
      <div><h1>Category Creator</h1>
        <Category {...categoryEvents} className="" tabIndex="0">
          Demo
        </Category>
        <Category {...categoryEvents} className="" tabIndex="0">
          Campsites
        </Category>
        &nbsp;
        <input tabIndex="0"/>
        <Button {...buttonEvents} className="some-class" kind="secondary">Save</Button>
        <Button {...buttonEvents} className="some-class" kind="default">Cancel</Button>
        &nbsp;
      </div>
  );
