// import { configure } from '@storybook/react';
// import infoAddon from '@storybook/addon-info';
//
//
// function loadStories() {
//   require('../stories/index.js');
//   // You can require as many stories as you need.
// }
//
// configure(loadStories, module);


import React from 'react';
import { configure, setAddon, addDecorator } from '@storybook/react';
import infoAddon from '@storybook/addon-info';
import Container from './Container';

// addDecorator(checkA11y);
addDecorator(story => <Container story={story} />);
setAddon(infoAddon);

function loadStories() {
  // require('../stories/index.js');
  // You can require as many stories as you need.
  const req = require.context('./components', true, /\.js$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
