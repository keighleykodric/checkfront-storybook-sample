import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  kind: PropTypes.oneOf(['default', 'primary', 'secondary']).isRequired,
  href: PropTypes.string,
  tabIndex: PropTypes.number,
  role: PropTypes.string,
};

const defaultProps = {
  tabIndex: 0,
  title: 'Name',
  type: 'catgeory',
  disabled: false,
  small: false,
  kind: 'default',
};

const Category = ({
  children,
  className,
  disabled,
  small,
  kind,
  href,
  tabIndex,
  ...other
}) => {
  const catgeoryClasses = classNames(className, {
    'category': true,
    'category--sm': small,
    'category--default': kind === 'default',
    'category--primary': kind === 'primary',
    'category--secondary': kind === 'secondary',
  });

  const commonProps = {
    tabIndex,
    className: catgeoryClasses,
  };

  const catgeory = (
    <catgeory {...other} {...commonProps} disabled={disabled}>
      {children}
    </catgeory>
  );


  return catgeory;
};

Category.propTypes = propTypes;
Category.defaultProps = defaultProps;

export default Category;
