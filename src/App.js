import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Category from './components/Category.js';
import Button from './components/Button.js';
import { categories, cats } from './data/data.js';

class App extends Component {
  state ={
    categories: [
      { name:'demo' },
      { name:"campsites" }
    ],
    show: false
  }

  renderCategoryCreator() {
    return (
      
    )
  }

  addCategory() {

  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <section className="">
          <Category className="">
            Demo
          </Category>
          <Category className="">
            Campsites
          </Category>

          <Button className="some-class" >Save</Button>
          <Button className="some-class" kind="secondary">Cancel</Button>
            {Object.keys(categories).map(category => (
              <Category key={category.name} title={category.name}>
                {categories.name}
              </Category>
        ))}


        </section>
        <p className="App-intro">

          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
